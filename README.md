---
title: README
author: Jan-Michael Rye
description: Niria theme README
---

# Synopsis

A simple Hugo theme for creating wikis and documentation websites for GitLab pages. The example website can be found [here](https://jrye.gitlabpages.inria.fr/hugo-theme-niria).


# Configuration

If you are new to Hugo, follow the official [Quick Start guide](https://gohugo.io/getting-started/quick-start/) to initialize a new site. To use this theme, run the following commands in your site's root directory:

~~~sh
git submodule add 'https://gitlab.inria.fr/jrye/hugo-theme-niria.git' themes/niria
# If you are on gitlab.inria.fr, you can use a relative path instead:
# git submodule add '../../jrye/hugo-theme-niria.git' themes/niria
~~~

and then update your `config.toml` file to use the theme "niria" instead of "ananke".


## config.toml

Here is an example of a minimal Hugo configuration file for this theme.

~~~toml
# The root URL of the generated website.
baseURL = 'https://foo.gitlabpages.inria.fr/wiki/'

# The website title.
title = 'Foo Wiki'

# Select the theme.
theme = 'niria'

# Specify the language code. The theme is not multilingual.
languageCode = 'en-us'

# Enable Git info to get page modification times from Git.
enableGitInfo = true

[Params]
# The base URL to concatenate with content filepaths to obtain a link to the
# source page in the corresponding online Git repository. This will provide
# source links at the top of each page for quick online content editing.
content_source_url = 'https://gitlab.inria.fr/foo/wiki/blob/main/content'

# The number of most recent updates to display at the bottom of the main page.
# Set this to 0 or delete the entry to remove the list from the page.
most_recent_updates = 10
~~~

## Content Organization

The theme assumes no particular content organization. Organize your files as you wish under the `content` directory. The hierarchy under the `content` directory will be reflected in the navigation panel, with pages under the same subdirectory ordered alphabetically.

## GitLab Pages

Add the following content to `.gitlab-ci.yml` in your Git repository's root directory to generate the GitLab pages website on `https://gitlab.inria.fr/` using the shared runners.

~~~yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  image: archlinux:latest

  tags:
    - ci.inria.fr
    - linux
    - small

  script:
    - pacman -Syu --noconfirm git hugo
    - hugo

  artifacts:
    paths:
      - public

  only:
    - main
~~~


## Further Reading

* [Configure Markup](https://gohugo.io/getting-started/configuration-markup/) - Documentation for configuring Markdown, syntax highlighting, table-of-contents generation, etc.
* [Syntax Highlighting](https://gohugo.io/content-management/syntax-highlighting/) - Documentation of Hugo's built-in `highlight` shortcode, which provides more control over code blocks than fenced code blocks in Markdown.


# TODO

* Add a custom shortcode to create links for page resources.
* Gradually improve the aesthetics of the theme.
