---
title: Homepage
description: Example homepage
---

## About

Niria is a very simple yet functional theme for the open-source static site generator [Hugo](https://gohugo.io/). It is mainly intended for generating simple wikis and technical documentation. The following is a list of some of its features:

* No structure is assumed or imposed on the site's `content` directory. The user is free to organize their content as they wish.
* CSS flex-boxes are used to provide a responsive web design that adapts to different screen sizes.
* The "Pages" navigation sidebar on the left provides a full overview of the site and direct access to all pages via a collapsible link hierarchy.
* The modification date of each page is displayed at the top of the page to immediately indicate to users the freshness of the page.
* If the `content_source_url` parameter is provided in the configuration file, a link to each page's source will be displayed at the top of the page under the modification time.
* The footer displays the date of the most recent site modification.

For anyone curious about the name, the original intention was to provide a theme that followed various recommendations of the Inria graphical charter but this was ultimately abandoned. The name was thus changed from "Inria" to "Niria" to leave a trace of this origin. The theme also retains the nuance of red from the official Inria color palette.

## Markdown

The Hugo website provides [links to learn Markdown](https://gohugo.io/content-management/formats/#learn-markdown). Check the Hugo documentation for up-to-date information about the specific flavor of Markdown that it supports.


## Example Page

See the [README page](readme) to see how the theme renders a full page with various elements such as code blocks and lists.
